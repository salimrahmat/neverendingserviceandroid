package oak.shef.ac.uk.testrunningservicesbackgroundrelaunched;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.SensorAdditionalInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pusher.java_websocket.client.WebSocketClient;
import com.pusher.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by fabio on 30/01/2016.
 */
public class SensorService extends Service {
    public int counter=0;
    private Activity activitynun;
    private clientWebSocket clientWebSocket;
    private ReceiveMessage receiveMessage = new ReceiveMessage();
    private Context context;
<<<<<<< HEAD
=======

    private boolean connected;
    private SensorService sensorService;
    private MainActivity mainActivity =  new MainActivity();
    Intent mServiceIntent;

    public SensorService(Activity activity) {
        this.activitynun = activity;
    }
>>>>>>> 2cac43966260328421306f69651d7cbed5641f82


//    public SensorService(Activity activity) {
//        this.activitynun = activity;
//    }

    public SensorService(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
    }

    public SensorService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplication();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        //startTimer();
        startSocket();
        checkInternet();
        return START_STICKY;
    }

    public void checkInternet(){
        if(AppStatus.getInstance(getApplicationContext()).isOnline()) {
            connected   = true;

            Toast.makeText(getApplicationContext(), "WiFi/Mobile Networks Connected!", Toast.LENGTH_SHORT).show();

            if (!isMyServiceRunning(getClass())) {

                startService(mServiceIntent);

            }

        } else {
            connected   = false;
            Toast.makeText(getApplicationContext(), "Ooops! No WiFi/Mobile Networks Connected!", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent(this, SensorRestarterBroadcastReceiver.class);
        sendBroadcast(broadcastIntent);
        checkInternet();
        //stoptimertask();
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (counter++));
            }
        };
    }


    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void startSocket() {
        try{
            clientWebSocket = new clientWebSocket(new URI("ws://apicarsurvey.baf.id:8980/pushnotif/notif/admin/1"));
            if(clientWebSocket.isOpen()){
                Log.d("Open","Still Opening");
            }else{
                clientWebSocket.connect();
            }
        } catch (URISyntaxException e) {
           e.printStackTrace();
        }

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public class clientWebSocket extends WebSocketClient{

        public clientWebSocket(URI serverURI) {
            super(serverURI);
        }

        @Override
        public void onOpen(ServerHandshake handshakedata) {
            Log.d("Open",handshakedata.toString());
        }

        @Override
        public void onMessage(String message) {
            Log.e("onMessage",message);
            HashMap h = new Gson().fromJson(message, HashMap.class);
            showPopUP(h.get("from")+"",h.get("content")+"");
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            Log.d("onClose",reason);
        }

        @Override
        public void onError(Exception ex) {
            Log.d("onError",ex.getMessage());
        }

    }

    private void showPopUP(final String head,final String content) {
         Thread thread = new Thread(new Runnable() {
             @Override
             public void run() {
                 try{
                     Thread.sleep(500);
                     receiveMessage.getMessage(head,content);
                 }catch (Exception a){
                     Log.d("error Thread",a.getMessage());
                 }
             }
         });thread.start();

    }

    public void sendMessage(final String to,final String message){
        try{
            clientWebSocket = new clientWebSocket(new URI("ws://apicarsurvey.baf.id:8980/pushnotif/notif/admin/1"));
            if(clientWebSocket.isOpen()){
                Log.d("Open","Still Opening");
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            Thread.sleep(500);
                            clientWebSocket.send("{content:'"+message+"',to:['"+to+"']}");
                        }catch (Exception a){
                            a.printStackTrace();
                            System.out.println("nugelo borok   "+message  +a.getMessage());
                        }
                    }
                });thread.start();
            }else{
                clientWebSocket.connect();
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            Thread.sleep(500);
                            clientWebSocket.send("{content:'"+message+"',to:['"+to+"']}");
                        }catch (Exception a){
                            a.printStackTrace();
                            System.out.println("nugelo borok   "+message  +a.getMessage());
                        }
                    }
                });thread.start();
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


    }
}