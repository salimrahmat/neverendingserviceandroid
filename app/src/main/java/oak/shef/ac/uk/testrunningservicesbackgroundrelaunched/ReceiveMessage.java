/*
 * Copyright (c) 2018. This code has been developed by Fabio Ciravegna, The University of Sheffield. All rights reserved. No part of this code can be used without the explicit written permission by the author
 */

package oak.shef.ac.uk.testrunningservicesbackgroundrelaunched;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by 0414831216 on 11/30/2018.
 */

public class ReceiveMessage {
    Context context = ApplicationGlobal.getAppContext();
    public void getMessage(final String head,final String message){

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(message.equals("Connected!")&& message.equals("Sent Successfully!") ){
                    Log.d("threas","show");
                }else{
                    /**used for deeplinking to other activities*/
                    Intent intent = new Intent(context, Activity_Kosong.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent, PendingIntent.FLAG_ONE_SHOT);

                   //        Intent intent = new Intent(ACTION_MSG_RECEIVED);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//                    Toast.makeText(context,message,Toast.LENGTH_SHORT).show();

                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                    /**define push notification pop up*/
                    Uri defaultSoundUri             = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder nb   = new android.support.v7.app.NotificationCompat.Builder(context)
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(head)
                            .setContentText(message)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                    notificationManager.notify(0, nb.build());



                }

            }
        });
    }

}
