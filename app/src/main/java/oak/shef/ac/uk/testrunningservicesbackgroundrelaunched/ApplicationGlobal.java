/*
 * Copyright (c) 2018. This code has been developed by Fabio Ciravegna, The University of Sheffield. All rights reserved. No part of this code can be used without the explicit written permission by the author
 */

package oak.shef.ac.uk.testrunningservicesbackgroundrelaunched;

import android.app.Application;
import android.content.Context;

/**
 * Created by 0414831216 on 12/3/2018.
 */

public class ApplicationGlobal extends Application {
    private static Context appContext;
    private static Application appAplication;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
    }


    public static Context getAppContext(){
        return appContext;
    }
}
