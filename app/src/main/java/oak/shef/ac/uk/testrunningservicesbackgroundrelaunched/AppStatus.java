/*
 * Copyright (c) 2018. This code has been developed by Fabio Ciravegna, The University of Sheffield. All rights reserved. No part of this code can be used without the explicit written permission by the author
 */

package oak.shef.ac.uk.testrunningservicesbackgroundrelaunched;

import android.content.Context;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by 0426611017 on 12/13/2018.
 */

public class AppStatus {

    static Context context;

    private static AppStatus instance   = new AppStatus();
    ConnectivityManager connectivityManager;
    boolean connected =  false;

    public static AppStatus getInstance(Context ctx){
        context = ctx.getApplicationContext();
        return instance;
    }

    public boolean isOnline(){
        try {
            connectivityManager     = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected   = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

            return connected;
        } catch (Exception e){
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
        }

        return connected;
    }
}
